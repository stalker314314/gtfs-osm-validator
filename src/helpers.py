import http
import os
import socket
import time
import urllib

from gtfspy import gtfs
from gtfspy import import_gtfs
from overpy.exception import OverpassTooManyRequests, OverpassGatewayTimeout


def load_or_import_example_gtfs(input_zip_filename, output_sqlite_filename, verbose=False):
    if not os.path.exists(output_sqlite_filename):  # reimport only if the imported database does not already exist
        print("Importing gtfs zip file. This can take couple of minutes, but it is needed only once.")
        import_gtfs.import_gtfs(
            [input_zip_filename, ],
            output_sqlite_filename,  # output: where to create the new sqlite3 database
            print_progress=verbose   # whether to print progress when importing data
        )

    # Now you can access the imported database using a GTFS-object as an interface:
    gtfs_db = gtfs.GTFS(output_sqlite_filename)

    if verbose:
        print("Location name:" + gtfs_db.get_location_name())
        print("Time span of the data in unixtime: " + str(gtfs_db.get_approximate_schedule_time_span_in_ut()))
    return gtfs_db


def retry_on_error(timeout_in_seconds=3*60):
    def decorate(func):
        def call(*args, **kwargs):
            retries = 5
            while retries > 0:
                try:
                    result = func(*args, **kwargs)
                except (ConnectionRefusedError, OverpassTooManyRequests, OverpassGatewayTimeout,
                        socket.timeout, urllib.error.URLError, http.client.RemoteDisconnected):
                    retries = retries - 1
                    print('Connection refused, retrying')
                    time.sleep(timeout_in_seconds)
                    continue
                return result
            raise Exception('Exhausted retries for connection refused, quitting')
        return call
    return decorate
